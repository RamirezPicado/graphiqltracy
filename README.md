# work-shop-server

npm init —y

npm install cors express express-graphql graphql mongose para agregar a package.json

crear server.js para agregar express de forma manual

npm run start

node server.js
http://localhost:4000/graphql
query {
  users {
    lastName
    id
    name
  }
}
  mutation {
	addUser (name: "Tracy", lastName: "Ramírez") {
    name
    lastName
  }
}